﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace QLThueNha
{
    public partial class Categories : Form
    {
        public Categories()
        {
            InitializeComponent();
            Showcategories();
        }
        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            this.Hide();
        }
        SqlConnection Con = new SqlConnection(Properties.Settings.Default.conString);
        private void Showcategories()
        {
            Con.Open();
            string Query = "Select * from categoryTbl";
            SqlDataAdapter sda = new SqlDataAdapter(Query, Con);
            SqlCommandBuilder Builder = new SqlCommandBuilder(sda);
            var ds = new DataSet();
            sda.Fill(ds);
            CategoriesDGV.DataSource = ds.Tables[0];
            Con.Close();
        }
        private void ResetData()
        {
            CategoryTb.Text = "";

            RemarksTb.Text = "";
        }

        private void AddBtn_Click(object sender, EventArgs e)
        {
            if (CategoryTb.Text == "" || RemarksTb.Text == "")
            {
                MessageBox.Show("Thiếu thông tin!!");
            }
            else
            {
                try
                {
                    Con.Open();
                    SqlCommand cmd = new SqlCommand("insert into CategoryTbl(Category,Remarks)values(@Cat,@Rem)", Con);
                    cmd.Parameters.AddWithValue("@Cat", CategoryTb.Text);
                    cmd.Parameters.AddWithValue("@Rem", RemarksTb.Text);

                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Đã them Category!!!");
                    Con.Close();
                    ResetData();
                    Showcategories();

                }
                catch (Exception Ex)
                {

                    MessageBox.Show(Ex.Message);
                }
            }
        }
    }
}
