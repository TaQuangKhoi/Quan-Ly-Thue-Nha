﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QLThueNha
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }
        private void Reset()
        {
            UnameTb.Text = "";
            PasswordTb.Text = "";
        }
        private void ResetBtn_Click(object sender, EventArgs e)
        {
            Reset();
        }

        private void LoginBtn_Click(object sender, EventArgs e)
        {
            if (UnameTb.Text == "" || PasswordTb.Text == "")
            {
                MessageBox.Show("Hãy điền đầy đủ thông tin!!!");
                Reset();
            }
            else if (UnameTb.Text == "Admin" && PasswordTb.Text == "Admin")
            {
                Tenants Obj = new Tenants();
                Obj.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("Sai thông tin tài khoản hoặc mật khẩu!!!");
                Reset();
            }
        }

        private void pictureBox_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
