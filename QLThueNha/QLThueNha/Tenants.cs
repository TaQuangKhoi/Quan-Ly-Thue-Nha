﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace QLThueNha
{
    public partial class Tenants : Form
    {
        public Tenants()
        {
            InitializeComponent();
            ShowTenants();
        }
        SqlConnection Con = new SqlConnection(Properties.Settings.Default.conString);

        private void ShowTenants()
        {
            Con.Open();
            string Query = "Select * from TenantTbl";
            SqlDataAdapter sda = new SqlDataAdapter(Query, Con);
            SqlCommandBuilder Builder = new SqlCommandBuilder(sda);
            var ds = new DataSet();
            sda.Fill(ds);
            TenantsDGV.DataSource = ds.Tables[0];
            Con.Close();
        }
        private void ResetData()
        {
            PhoneTb.Text = "";
            GenCb.SelectedIndex = -1;
            TNameTb.Text = "";
        }
        private void Savebtn_Click(object sender, EventArgs e) //thêm tenants
        {
            if (TNameTb.Text == "" || GenCb.SelectedIndex == -1 || PhoneTb.Text == "")
            {
                MessageBox.Show("Thiếu thông tin!!!");
            }
            else
            {
                try
                {
                    Con.Open();
                    SqlCommand cmd = new SqlCommand("insert into TenantTbl(TenName,TenPhone,TenGen)values(@TN,@TP,@TG)", Con);
                    cmd.Parameters.AddWithValue("@TN", TNameTb.Text);
                    cmd.Parameters.AddWithValue("@TP", PhoneTb.Text);
                    cmd.Parameters.AddWithValue("@TG", GenCb.SelectedItem.ToString());
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Đã thêm người thuê");
                    Con.Close();
                    ResetData();
                    ShowTenants();
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.Message);
                }
            }
        }
        int Key = 0;

        private void TenantsDGV_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            TNameTb.Text = TenantsDGV.SelectedRows[0].Cells[1].Value.ToString();
            PhoneTb.Text = TenantsDGV.SelectedRows[0].Cells[2].Value.ToString();
            GenCb.Text = TenantsDGV.SelectedRows[0].Cells[3].Value.ToString();
            if (TNameTb.Text == "")
            {
                Key = 0;
            }
            else
            {
                Key = Convert.ToInt32(TenantsDGV.SelectedRows[0].Cells[0].Value.ToString());
            }
        }

        private void DeleteBtn_Click(object sender, EventArgs e) //Xóa tenants
        {
            if(Key == 0)
            {
                MessageBox.Show("Chọn người may mắn bị diss!");
            }
            else
            {
                try
                {
                    Con.Open();
                    SqlCommand cmd = new SqlCommand("delete from TenantTbl where TenID=@TKey", Con);
                    cmd.Parameters.AddWithValue("@Tkey", Key);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Tanant Deleted!");
                    Con.Close();
                    ResetData();
                    ShowTenants();
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.Message);
                }
            }
        }

        private void EditBtn_Click(object sender, EventArgs e)
        {
            if (TNameTb.Text == "" || GenCb.SelectedIndex == -1 || PhoneTb.Text == "")
            {
                MessageBox.Show("Thiếu thông tin!");
            }
            else
            {
                try
                {
                    Con.Open();
                    SqlCommand cmd = new SqlCommand("update TenantTbl set TenName=@TN,TenPhone=@TP,TenGen=@TG where TenID=@TKey", Con);
                    cmd.Parameters.AddWithValue("@TN", TNameTb.Text);
                    cmd.Parameters.AddWithValue("@TP", PhoneTb.Text);
                    cmd.Parameters.AddWithValue("@TG", GenCb.SelectedItem.ToString());
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Đã cập nhật người thuê");
                    Con.Close();
                    ResetData();
                    ShowTenants();
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.Message);
                }
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {
            Apartments Obj = new Apartments();
            Obj.Show();
            this.Hide();
        }

        private void label4_Click(object sender, EventArgs e)
        {
            LandLords Obj = new LandLords();
            Obj.Show();
            this.Hide();
        }

        private void label5_Click(object sender, EventArgs e)
        {
            Rents Obj = new Rents();
            Obj.Show();
            this.Hide();
        }
    }
}
