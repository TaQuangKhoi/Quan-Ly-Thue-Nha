﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace QLThueNha
{
    public partial class Rents : Form
    {
        public Rents()
        {
            InitializeComponent();
            GetApart();
            GetTenant();
            ShowRents();
        }
        SqlConnection Con = new SqlConnection(Properties.Settings.Default.conString);
        private void GetApart()
        {
            Con.Open();
            SqlCommand cmd = new SqlCommand("select ANum from ApartTbl", Con);
            SqlDataReader Rdr;
            Rdr = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.Add("ANum", typeof(int));
            dt.Load(Rdr);
            ApartCb.ValueMember = "ANum";
            ApartCb.DataSource = dt;
            Con.Close();
        }
        private void GetTenant()
        {
            Con.Open();
            SqlCommand cmd = new SqlCommand("select TenId from TenantTbl", Con);
            SqlDataReader Rdr;
            Rdr = cmd.ExecuteReader();
            DataTable dt = new DataTable();
            dt.Columns.Add("TenId", typeof(int));
            dt.Load(Rdr);
            TenantCb.ValueMember = "TenId";
            TenantCb.DataSource = dt;
            Con.Close();
        }
        private void ShowRents()
        {
            Con.Open();
            string Query = "Select * from RentTbl";
            SqlDataAdapter sda = new SqlDataAdapter(Query, Con);
            SqlCommandBuilder Builder = new SqlCommandBuilder(sda);
            var ds = new DataSet();
            sda.Fill(ds);
            RentsDGV.DataSource = ds.Tables[0];
            Con.Close();
        }
        private void ResetData()
        {
            AmountTb.Text = "";
        }
        private void GetCost()
        {
            Con.Open();
            string Query = "select * from ApartTbl where ANum=" + ApartCb.SelectedValue.ToString() + "";
            SqlCommand cmd = new SqlCommand(Query, Con);
            DataTable dt = new DataTable();
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            sda.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                AmountTb.Text = dr["ACost"].ToString();
            }
            Con.Close();
        }
        private void pictureBox4_Click(object sender, EventArgs e) //Thêm
        {
            if (ApartCb.SelectedIndex == -1 || TenantCb.SelectedIndex == -1 || AmountTb.Text == "")
            {
                MessageBox.Show("Thiếu thông tin!!!");
            }
            else
            {
                try
                {
                    string Period = RDate.Value.Date.Month + "-" + RDate.Value.Date.Year;
                    Con.Open();
                    SqlCommand cmd = new SqlCommand("insert into RentTbl(Apartment,Tenant,Period,Amount)values(@RA,@RT,@RP,@RAm)", Con);
                    cmd.Parameters.AddWithValue("@RA", ApartCb.SelectedValue.ToString());
                    cmd.Parameters.AddWithValue("@RT", TenantCb.SelectedValue.ToString());
                    cmd.Parameters.AddWithValue("@RP", Period);
                    cmd.Parameters.AddWithValue("@RAm", AmountTb.Text);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Đã thêm tiền thuê!!!");
                    Con.Close();
                    ResetData();
                    ShowRents();

                }
                catch (Exception Ex)
                {

                    MessageBox.Show(Ex.Message);
                }
            }
        }
        private void ApartCb_SelectionChangeCommitted(object sender, EventArgs e)
        {
            GetCost();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            Tenants Obj = new Tenants();
            Obj.Show();
            this.Hide();
        }

        private void label3_Click(object sender, EventArgs e)
        {
            Apartments Obj = new Apartments();
            Obj.Show();
            this.Hide();
        }

        private void label4_Click(object sender, EventArgs e)
        {
            LandLords Obj = new LandLords();
            Obj.Show();
            this.Hide();
        }

        private void label6_Click(object sender, EventArgs e)
        {
            Categories Obj = new Categories();
            Obj.Show();
        }
    }
}
